# Java Barebones Authorization App #

### What you will need
 * The protoc compiler on your workstation (Google's [protocol buffer compiler](https://github.com/google/protobuf/releases))
 * Mastercard Developers credential
  * **If you dont already have a P12 / Consumerkey for the Blockchain API you can obtain one from [Mastercard Developers](https://developers.mastercard.com)** 
 * Blockchain App Id 
  * **If you dont already have this you obtain one from [Mastercard Developers](https://developers.mastercard.com) by creating a Blockchain project**   

## Getting Started ##
A sample use-case of a application that can make authorization requests. To get started you should take the following steps

 * Clone this repository
 * Execute the following commands
```bash
mvn package
java -jar target/java-blockchain-barebones-authorizeapp-0.0.1-SNAPSHOT-jar-with-dependencies -kp <path to p12> -ck <your consumer key>
```

When started it gets you to confirm your parameters and then displays a simple menu. 

## Menu ##
```
============ MENU ============
1. Create node (optional, onetime)
2. Update protocol buffer definition
3. Create authorization request
4. Show Protocol Buffer Definition
5. Re-initialize API
6. Print Command Line Options
0. Quit
Option [0]: 
```

## More Commandline Options ##
```
============ COMMAND LINE OPTIONS ============
usage: java -jar <jarfile>
 -ck,--consumerKey <arg>    consumer key (mastercard developers)
 -ka,--keyAlias <arg>       key alias (mastercard developers)
 -kp,--keystorePath <arg>   the path to your keystore (mastercard
                            developers)
 -sp,--storePass <arg>      keystore password (mastercard developers)
 -v,--verbosity             log mastercard developers sdk to console
```

## Useful Info ##
This project makes use of the Mastercard Blockchain SDK available from mvn.

```xml
<dependency>
	<groupId>com.mastercard.api</groupId>
	<artifactId>blockchain</artifactId>
	<version>0.0.2</version>
</dependency>

```
